package com.example.T1.modelsTest;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class DoctorsOfficeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DoctorsOfficeRepository repo;

    @Test
    public void testCreateOffice() {
        DoctorsOffice office = new DoctorsOffice();
        office.setName("ORL Gorj");
        office.setSpecialty("ORL");
        office.setDoctorName("Braia Carmen");
        office.setControlPrice(150);
        office.setCas("Yes");
        office.setAvailableAppointments(4);

        DoctorsOffice savedOffice = repo.save(office);

        DoctorsOffice existOffice = entityManager.find(DoctorsOffice.class, savedOffice.getId());

        assertThat(office.getName()).isEqualTo(existOffice.getName());
    }
}
