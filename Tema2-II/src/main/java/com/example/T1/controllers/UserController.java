package com.example.T1.controllers;

import com.example.T1.models.User;
import com.example.T1.services.implementation.UserServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  In clasa UserController sunt implementate metodele de apel corespunzatoare unui User,
 *  facandu-se modificari in baza de date.
 *  Sunt apelate metodele din Service.
 *
 */
@RestController
public class UserController {

    @Autowired
    UserServiceLogic userService;

    /**
     * Metoda returnUser() va returna lista cu toti utilizatorii aplicatiei.
     * @return lista de utilizatori
     */
    @GetMapping("/getAllUsers")
    public List<User> returnUsers ()
    {

        return userService.returnAllUsers();
    }

    /**
     * Metoda putUser() va insera un nou utilizator in tabelul "user".
     * @param firstUser - Entry-ul in tabelul "user"(noul utilizator)
     */
    @PostMapping(path="/postUser", consumes="application/json", produces = "application/json")
    public void putUser (@RequestBody User firstUser)
    {

        userService.putAnUser(firstUser);
    }



}
