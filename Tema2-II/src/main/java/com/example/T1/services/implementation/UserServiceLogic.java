package com.example.T1.services.implementation;


import com.example.T1.facade.UserFacade;
import com.example.T1.models.User;
import com.example.T1.repositories.DoctorsOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * Clasa UserServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 * Foloseste metodele din Facade, fara a utiliza direct Repository-ul.
 */

@Service
public class UserServiceLogic{

    @Autowired
    UserFacade userFacade;

    /**
     * Constructorul clasei, ce primeste ca parametru obiectul de clasa Facade pentru User
     */
    public UserServiceLogic() {

    }

    public UserServiceLogic(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    /**
     * Metoda verifyUserName() are rolul de a verifica unicitatea
     * unui User in functie de userame.
     * @param name - username-ul cautat
     * @return true daca nu mai exista un User cu acelasi username, false in caz contrar
     */
    public boolean verifyUserName(String name){

        if( userFacade.findByUsernameUser(name) == null )
        return true;

     return false;
    }

    public List<User> returnAllUsers(){
        return userFacade.returnUsers();
    }

    public User putAnUser (User firstUser)
    {

        if(this.verifyUserName(firstUser.getUsername()))
            return userFacade.putAnUser(firstUser);

        return null;
    }


}