package com.example.T1.repositories;

import com.example.T1.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Clasa UserRepository face conexiunea cu tabelul user din baza de date.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Functia findByUsername() are ca implementare Query-ul ce selecteaza un entry
     * din tabelul user in functie de username.
     * @param username - username-ul user-ului
     * @return entry-urile care respecta conditia
     */
    @Query("SELECT u FROM User u WHERE u.username = ?1")
    public User findByUsername(String username);

}