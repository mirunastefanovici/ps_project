package com.example.T1.controllers;

import com.example.T1.models.User;
import com.example.T1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepository anUser;

    @GetMapping("/getAllUsers")
    public List<User> returnUser ()
    {
        return anUser.findAll();
    }

    @PostMapping(path="/postUser", consumes="application/json", produces = "application/json")
    public User putUser (@RequestBody User firstUser)
    {

        return anUser.save(firstUser);
    }
}
