package com.example.T1.controllers;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DoctorsOfficeController {

    @Autowired
    DoctorsOfficeRepository anOffice;

    @GetMapping("/getAllOffices")
    public List<DoctorsOffice> returnUser ()
    {
        return anOffice.findAll();
    }

    @PostMapping(path="/postOffice", consumes="application/json", produces = "application/json")
    public DoctorsOffice putUser (@RequestBody DoctorsOffice firstOffice)
    {

        return anOffice.save(firstOffice);

    }
}
