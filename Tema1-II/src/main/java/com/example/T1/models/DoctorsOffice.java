package com.example.T1.models;

import javax.persistence.*;

@Entity
@Table
public class DoctorsOffice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false, length = 45)
    private String name;

    @Column(name = "specialty", nullable = false, length = 45)
    private String specialty ;

    @Column(nullable = false, length = 45)
    private String doctorName;

    @Column(nullable = false)
    private int controlPrice;

    @Column(nullable = false, length = 45)
    private String cas;

    @Column(nullable = false)
    private int availableAppointments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public int getControlPrice() {
        return controlPrice;
    }

    public void setControlPrice(int controlPrice) {
        this.controlPrice = controlPrice;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public int getAvailableAppointments() {
        return availableAppointments;
    }

    public void setAvailableAppointments(int availableAppointments) {
        this.availableAppointments = availableAppointments;
    }
}