package com.example.T1.changesTest;


import com.example.T1.models.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;


public class AppointmentsChangesTests {

    @Test
    public void testSpecialtyReportType()
    {
        ReportFactory reportFactory = new ReportFactory();
        Report report = reportFactory.createReport(null, null, null,ReportChoose.Specialty);
        Assertions.assertTrue(report instanceof SpecialtyReports);

    }

    @Test
    public void testSpecialtyReportString()
    {
        ReportFactory reportFactory = new ReportFactory();
        List<DoctorsOffice>  allOfficesWithAspecialty = new ArrayList<>();

        DoctorsOffice office1 = new DoctorsOffice();
        office1.setId((long) 1);
        office1.setAvailableAppointments(5);
        office1.setCas("yes");
        office1.setControlPrice(100);
        office1.setSpecialty("Cardiologie");
        office1.setName("CardiologieA");
        office1.setDoctorName("Neamtu TatianaA");

        DoctorsOffice office2 = new DoctorsOffice();
        office2.setId((long) 2);
        office2.setAvailableAppointments(5);
        office2.setCas("yes");
        office2.setControlPrice(100);
        office2.setSpecialty("Cardiologie");
        office2.setName("CardiologieB");
        office2.setDoctorName("Neamtu TatianaB");

        allOfficesWithAspecialty.add(office1);
        allOfficesWithAspecialty.add(office2);

        Report report = reportFactory.createReport(null, null, allOfficesWithAspecialty,ReportChoose.Specialty);
        Assertions.assertEquals("Cabinetele medicale cu aceasta specialitate sunt:"+'\n'+"CardiologieA"+'\n'+"CardiologieB"+'\n'
                , report.getChooseReport());


    }
    @Test
    public void testAppointmentReportType()
    {
        ReportFactory reportFactory = new ReportFactory();
        Report report = reportFactory.createReport(null, null, null,ReportChoose.Appointments);
        Assertions.assertTrue(report instanceof AppointmentsReport);

    }




}
