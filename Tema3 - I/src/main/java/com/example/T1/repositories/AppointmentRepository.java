package com.example.T1.repositories;
import com.example.T1.models.Appointments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointments, Long>{

    @Query("SELECT u FROM Appointments u WHERE u.id_user = ?1")
    public Appointments findByUserId(Long idUser);

}
