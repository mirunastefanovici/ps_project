package com.example.T1.controllers;
import com.example.T1.services.implementation.ReportServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReportController {

    @Autowired
    ReportServiceLogic reportServiceLogic;

    @GetMapping(path="/reportSpecialty/{specialty}")
    public String getReportSpecialty(@PathVariable String specialty)
    {
        return reportServiceLogic.getSpecialtyReport(specialty);
    }

    @GetMapping(path="/reportAppointment/{id_user}")
    public String getReportSpecialty(@PathVariable Long id_user)
    {
        return reportServiceLogic.getAppointmentsReport(id_user);
    }
}
