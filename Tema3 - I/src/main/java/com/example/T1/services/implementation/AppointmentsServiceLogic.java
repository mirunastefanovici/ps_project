package com.example.T1.services.implementation;

import com.example.T1.facade.AppointmentsFacade;
import com.example.T1.models.Appointments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Clasa AppointmentsServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 * Foloseste metodele din Facade, fara a utiliza direct Repository-ul.
 */
@Service
public class AppointmentsServiceLogic{

    @Autowired
    AppointmentsFacade appointmentsFacade;

    public AppointmentsServiceLogic() {

    }

    public AppointmentsServiceLogic(AppointmentsFacade appointmentsFacade) {
        this.appointmentsFacade = appointmentsFacade;
    }

    public List<Appointments> returnAllAppointments(){
        return appointmentsFacade.returnAppointments();
    }

    public Appointments putAnAppointment (Appointments appointment)
    {
            return appointmentsFacade.putAnAppointment(appointment);

    }

    public void deleteAllAppointments() {
        appointmentsFacade.deleteAllAppointments();
    }


}