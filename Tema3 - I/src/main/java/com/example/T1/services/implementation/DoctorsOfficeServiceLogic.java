package com.example.T1.services.implementation;


import com.example.T1.facade.DoctorsOfficeFacade;
import com.example.T1.facade.UserFacade;
import com.example.T1.models.DoctorsOffice;
import com.example.T1.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Clasa DoctorsOfficeServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 * Foloseste metodele din Facade, fara a utiliza direct Repository-ul.
 *
 */
@Service
public class DoctorsOfficeServiceLogic {

    @Autowired
    DoctorsOfficeFacade doctorsOfficeFacade;

    @Autowired
    private UserFacade userFacade;

    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public DoctorsOfficeServiceLogic() {

    }

    public DoctorsOfficeServiceLogic(DoctorsOfficeFacade doctorsOfficeFacade) {
        this.doctorsOfficeFacade = doctorsOfficeFacade;
    }

    public DoctorsOfficeServiceLogic(DoctorsOfficeFacade doctorsOfficeFacade,UserFacade userFacade ) {
        this.doctorsOfficeFacade = doctorsOfficeFacade;
        this.userFacade = userFacade;
    }
    /**
     * Metoda verifyDoctorsOfficeName() are rolul de a verifica unicitatea
     * unui cabinet medical in functie de nume.
     * @param name - numele cautat
     * @return true daca nu mai exista un cabinet cu acelasi nume, false in caz contrar
     */

    public boolean verifyDoctorsOfficeName(String name){

        DoctorsOffice theOffice = doctorsOfficeFacade.findByName(name);
        if( theOffice == null)
            return true;
        return false;
    }

    /**
     * Lista de observeri va fi alcatuita din toti userii din baza de date.
     */
    public List<User> putUsersFromdatabase(){

        users = userFacade.returnUsers();

        return users;
    }

    public List<DoctorsOffice> returnAllOffices(){
        return doctorsOfficeFacade.returnAllOffices();
    }

    public Optional<DoctorsOffice> returnOfficeById(Long id) {

        return doctorsOfficeFacade.returnOfficeById(id);

    }

    public DoctorsOffice returnOfficeByCAS(String cas) {

        return doctorsOfficeFacade.returnOfficeByCAS(cas);

    }

    /** Fiecare user din lista cu toti userii va fi notificat cand este adaugat un nou cabinet.
     * */
    public DoctorsOffice putOffice (DoctorsOffice firstOffice)
    {
        if(this.verifyDoctorsOfficeName(firstOffice.getName())) {
            users = this.putUsersFromdatabase();
           for(User user : users)
               user.update();
           return doctorsOfficeFacade.putAnOffice(firstOffice);
        }

        return null;
    }

    public void replaceOffice(DoctorsOffice newOffice, Long id) {

        doctorsOfficeFacade.replaceOffice(newOffice,id);
    }

    public void deleteOffice(Long id) {
        doctorsOfficeFacade.deleteOffice(id);
    }

    public void deleteAllOffices() {
        doctorsOfficeFacade.deleteAllOffices();
    }

}
