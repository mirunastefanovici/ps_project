package com.example.T1.models;

import java.util.List;

/**
 * Clasa SpecialtyReports va extinde clasa Report si va avea propria implementare a metodei abstacte getChooseReport().
 *
 */
public class SpecialtyReports extends Report{

    public SpecialtyReports(List<User> theUsers, List<Appointments> theAppointmentsForEachUser,
                            List<DoctorsOffice>  allOfficesWithAspecialty, ReportChoose reportChoose)
    {
        super(theUsers,theAppointmentsForEachUser,allOfficesWithAspecialty,reportChoose);

    }


    /**
     * Metoda returneaza un String ce contine toate numee cabinetelor medicale de o anumita specialitate.
     *
     */
    @Override
    public String getChooseReport()
    {
        String result="";
        result = result + "Cabinetele medicale cu aceasta specialitate sunt:"+'\n';
        for(DoctorsOffice office : this.getAllOfficesWithAspecialty())
        {
            result = result + office.getName()+'\n';

        }

        return result;
    }
}
