package com.example.T1.models;


import java.util.List;

/**
 * Clasa abstracta reprezentativa pentru ideea generala de Report.
 * Va contine listele necesare formarilor celor doua clase copii.
 */
public abstract class Report {
    private List<User> theUsers;
    private List<Appointments> theAppointmentsForEachUser;
    private List<DoctorsOffice>  allOfficesWithAspecialty;
    private ReportChoose chooseReport;


    public Report()
    {

    }

    public Report(List<User> theUsers, List<Appointments> theAppointmentsForEachUser, List<DoctorsOffice> allOfficesWithAspecialty, ReportChoose chooseReport) {
        this.theUsers = theUsers;
        this.theAppointmentsForEachUser = theAppointmentsForEachUser;
        this.allOfficesWithAspecialty = allOfficesWithAspecialty;
        this.chooseReport = chooseReport;

    }

    public List<User> getTheUsers() {
        return theUsers;
    }

    public void setTheUsers(List<User> theUsers) {
        this.theUsers = theUsers;
    }

    public List<Appointments> getTheAppointmentsForEachUser() {
        return theAppointmentsForEachUser;
    }

    public void setTheAppointmentsForEachUser(List<Appointments> theAppointmentsForEachUser) {
        this.theAppointmentsForEachUser = theAppointmentsForEachUser;
    }

    public List<DoctorsOffice> getAllOfficesWithAspecialty() {
        return allOfficesWithAspecialty;
    }

    public void setAllOfficesWithAspecialty(List<DoctorsOffice> allOfficesWithAspecialty) {
        this.allOfficesWithAspecialty = allOfficesWithAspecialty;
    }

    public abstract String getChooseReport();

}
