package com.example.T1.models;

import javax.persistence.*;

/**
 * Appointments este clasa reprezentanta pentru informatiile legate de programarile utilizatorilor aplicatiei.
 * Fiecare programare va avea: un id_programare, id-ul user-ului, id-ul cabinetului medical la care este facuta programarea.
 *
 */
@Entity
@Table
public class Appointments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_appointment;

    @Column(nullable = false)
    private Long id_office;

    @Column(nullable = false)
    private Long id_user;

    public Long getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(Long id_appointment) {
        this.id_appointment = id_appointment;
    }

    public Long getId_office() {
        return id_office;
    }

    public void setId_office(Long id_office) {
        this.id_office = id_office;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }
}
