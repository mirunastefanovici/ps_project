package com.example.T1.models;

import java.util.List;

/**
 * Clasa AppointmentsReport va extinde clasa Report si va avea propria implementare a metodei abstacte getChooseReport().
 *
 */
public class AppointmentsReport extends Report{

    public AppointmentsReport(List<User> theUsers, List<Appointments> theAppointmentsForEachUser,
                            List<DoctorsOffice>  allOfficesWithAspecialty, ReportChoose reportChoose)
    {
        super(theUsers,theAppointmentsForEachUser,allOfficesWithAspecialty,reportChoose);
    }

    /**
     * Metoda returneaza un String ce contine toate id-urile cabinetelor medicale la care e programat un anumit user.
     *
     */
    @Override
    public String getChooseReport()
    {
        String result="";
        result = result + "User-ul are programare la urmatoarele cabinete medicale:"+'\n';

        for(Appointments appointments : this.getTheAppointmentsForEachUser())
        {
            result = result + "-cabinetul cu id-ul:"+appointments.getId_office()+'\n';

        }
        return result;
    }
}
