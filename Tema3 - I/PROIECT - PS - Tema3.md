# PROIECT - PS
## Aplicatie pentru programari medicale

     Aplicatia are rolul de a aduce un ajutor in plus in procesul
    de cautare al unui loc disponibil pentru un control la un cabinet in
    functie de problemele medicale.

## Functionalitati:

- creare cont si autentificare
- vizualizarea specializarilor medicale pentru care sunt valide programarile
- vizualizarea cabinetelor medicale si a informatiilor corespunzatoare acestora
- crearea unei programari pentru un loc disponibil
- vizualizarea unei pagini cu scop informativ despre anumite probleme medicale

## Implementare initiala 
     Aplicatia utilizeaza RESTful API, respectand arhitectura REST ce are la baza
    o serie de reguli din punct de vedere arhitectural.
    
**Implementarea logicii este structurata pe pachete**:
- pachetul "controllers" : clasele UserController, DoctorsOfficeControler
- pachetul "models" : clasele User, DoctorsOffice
- pachetul "repositories": clasele UserRepository, DoctorsOfficeRepository
- pachetul "services" ce contine subpachetele "implementation", "interfaces"
- pachetul "implementation": clasele UserServiceLogic, DoctorsOfficeServiceLogic
- pachetul "changesTest" : clasele DoctorsOfficeChangesTest, UserChangesTes
- pachetul "modelsTest" : clasele UserRepositoryTest, DoctorsOfficeRepositoryTest
- pachetul "facade" : clasele UserFacade, DoctorsOfficeFacade

## **Pachetul MODELS**
     Clasele User si DoctorOffice sunt clasele reprezentante pentru informatiile legate
    de utilizatori si cabinetele medicale disponibile.
    
     In clasa User este implementata metoda de update, fiecare user find un observer.
## **Pachetul CONTROLLERS**
     Clasele UserController si DoctorsOfficeControler sunt clasele in care am implementat
    metodele de apel corespunzatoare acestor entitati : GET, POST, PUT, DELETE.
    
     In urma apelurilor, se vor observa modificarile aferente metodelor apelate.
## **Pachetul REPOSITORIES**
      Clasele UserRepository si DoctorsOfficeRepository fac conexiunea cu tabelele
     din baza de date. Pe langa metodele predefinite, am implementat si metode noi 
     in functie de necesitatile impuse de logica aplicatiei. De exemplu, una dintre aceste
     metode este findByCAS() ce selecteaza toate cabinetele inscrise in tabel ce au contract
     cu CAS.
## **Pachetul SERVICES**
     Clasele UserServiceLogic si DoctorsOfficeServiceLogic au rolul de a separa
    logica verificarilor de logica din Controller, ajutand la o mai buna organizare si
    intelegere a responsabilitatilor fiecarei entitati. 
## **Pachetul FACADE**
      Clasele UserFacade, DoctorsOfficeFacade contin logica de baza pentru fiecare
     Repository : metode de get, post, delete, etc. Metodele din facade vor fi apelate
     in service pentru logica de business.
## **Pachetul CHANGESTEST**
     Acest pachet contine clase de test in care sunt implementate teste ce verifica logica
    din clasele de service. Se verifica apelarea metodelor findByName(), findByUsername(), 
    save(). De asemenea, se va testa si Observer-ul in metoda testObserver() prin 
    verificarea flow-ului aplicatiei : se testeaza daca metoda update() va fi apelata 
    pentru fiecare user.

## **ENDPOINTS**
    Aplicatia ar functiona in felul urmator:
    - Utilizatorul isi va face cont si va fi adaugat in baza de date.
    - In functie de cabinetul dorit, specializare sau CAS, utilizatorul isi va face 
      o programare.
    - In urma programarii, userul va fi redirectionat la pagina cu lista de cabinete.
    Endpoint-urile au denumiri sugestive.
    
    ->Listare endpoint-uri:
    In controller-ul pentru DoctorsOffice :
        - /getAllOffices -> returnarea tuturor cabinetelor
        - /getOfficeById/{id} -> returnare cabinet in functie de id
        - /getOfficeByCAS/{cas} -> returnare cabinet in functie de CAS
        - /postOffice -> adaugarea unui cabinet
        - /updateOffice/{id} -> update in functie de id
        - /deleteOffice/{id} -> stergere dupa id
        - /deleteAllOffices -> stergerea tuturor cabinetelor
    In controller-ul pentru User :
        - /getAllUsers -> returnarea tuturor userilor
        - /postUser -> adaugarea unui user
    In controller-ul pentru Appointments :
        - /getAllAppointments -> returnarea tuturor programarilor
        - /postAppointment -> adaugarea unui appointment
        -/deleteAllApointments -> stergerea programarilor
        
    
     Proiectul utilizeaza Factory Pattern in contextul emiterii anumitor reports in functie de necesitati. Acest pattern ajuta la problema prastrarii abstractizarii, fara a specifica exact clasa obiectului care se doreste a fi creat.
     
     Ideea implementarii acestui design pattern in contextul aplicatiei este urmatoarea:
     - Utilizatorul va dori sa vizualizeze toate cabinetele care au o anumita specialitate
     si de asemenea, lista cabinetelor la care acesta s-a programat.
     - Aceste listari sunt reprezentate de cele doua tipuri de reports: Specialty si Appointments.
     - In noua clasa de Service corespunzatoare clasei Report este implementata logica
     pentru listarea informatiilor cerute.
     - Astfel, in urma apelarii in Service a metodei createReport() din ReportFactory se va crea tipul respectiv de report.

  
**Concluzii - tema 1:**
- Am reusit conexiunea cu baza de date.
- Am reusit sa testez metodele de apel pe baza de date.
- Am reusit sa fac teste pentru apelarea unor metode si pentru inserarea in baza de date.
- Poate exista o evolutie ulterioara.

**Concluzii - tema 2:**
- Am reusit sa adaug conceptul de Observer.
- Am reusit implementarea layer-ului Facade.
- Testarea Observer-ului.

**Concluzii - tema 3:**
- Am reusit sa adaug conceptul de Factory Pattern.
- Design Pattern-ul a fost folosit in contextul emiterii unor reports.
- Testarea crearii acestor tipuri de reports.

    

                                                    Stefanovici Miruna-Andreea
    
    
    





















