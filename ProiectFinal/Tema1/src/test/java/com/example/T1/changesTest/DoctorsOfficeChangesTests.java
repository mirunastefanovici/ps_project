package com.example.T1.changesTest;

import com.example.T1.facade.DoctorsOfficeFacade;
import com.example.T1.facade.UserFacade;
import com.example.T1.models.DoctorsOffice;
import com.example.T1.models.User;
import com.example.T1.repositories.DoctorsOfficeRepository;
import com.example.T1.repositories.UserRepository;
import com.example.T1.services.implementation.DoctorsOfficeServiceLogic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertTrue;


@ExtendWith(MockitoExtension.class)
public class DoctorsOfficeChangesTests {

    @Mock
    DoctorsOfficeRepository doctorsOfficeRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    List<User> users;
    @Test
    public void testObserver()
    {
        DoctorsOfficeFacade officeFacade = new DoctorsOfficeFacade(doctorsOfficeRepository);
        UserFacade userFacade = new UserFacade(userRepository);
        DoctorsOfficeServiceLogic officeService = new DoctorsOfficeServiceLogic(officeFacade,userFacade );
        DoctorsOffice office = new DoctorsOffice();

        office.setId((long) 2);
        office.setAvailableAppointments(5);
        office.setCas("yes");
        office.setControlPrice(100);
        office.setSpecialty("Cardiologie");
        office.setName("ORL Gorj");
        office.setDoctorName("Neamtu Tatiana");

        users = officeService.putUsersFromdatabase();
        officeService.putOffice(office);

        for(User anUser : users)
            verify(anUser, atLeastOnce()).update();

        /*for(User anUser : officeService.getUsers())
            assertTrue("Test Observer", anUser.getVerifyObserver());*/

    }
    @Test
    public void testSameName()
    {

        DoctorsOfficeFacade officeFacade = new DoctorsOfficeFacade(doctorsOfficeRepository);
        DoctorsOfficeServiceLogic officeService = new DoctorsOfficeServiceLogic(officeFacade);
        DoctorsOffice office = new DoctorsOffice();

        office.setId((long) 2);
        office.setAvailableAppointments(5);
        office.setCas("yes");
        office.setControlPrice(100);
        office.setSpecialty("Cardiologie");
        office.setName("ORL Gorj");
        office.setDoctorName("Neamtu Tatiana");

        boolean check = officeService.verifyDoctorsOfficeName(office.getName());
        verify(doctorsOfficeRepository, atLeastOnce()).findByName(office.getName());
    }

    @Test
    public void testSavingDoctorOffice(){
        doctorsOfficeRepository = mock(DoctorsOfficeRepository.class);

        DoctorsOffice office = new DoctorsOffice();
        office.setId((long) 2);
        office.setAvailableAppointments(5);
        office.setCas("yes");
        office.setControlPrice(100);
        office.setSpecialty("Cardiologie");
        office.setName("ORL Gorj");
        office.setDoctorName("Neamtu Tatiana");

        doctorsOfficeRepository.save(office);
        verify(doctorsOfficeRepository,atLeastOnce()).save(office);
    }

}
