package com.example.T1.models;

/**
 * Tipurile de reports necesare
 */
public enum ReportChoose {
    Specialty, Appointments
}
