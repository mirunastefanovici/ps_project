package com.example.T1.models;

import java.util.List;

/**
 * Clasa ReportFactory este clasa in care se implementeaza pattern-ul Factory.
 *
 */
public class ReportFactory {

    /**
     * In functie de o serie de parametrii decidem ce fel de obiect se va crea.
     * @param theUsers - lista de useri
     * @param theAppointmentsForEachUser - lista cu programarile unui user
     * @param allOfficesWithAspecialty - lista cu numele cabinetelor de o anumita specialitate
     * @param reportChoose - tipul de report
     * @return report-ul ales
     */
    public Report createReport(List<User> theUsers,  List<Appointments> theAppointmentsForEachUser,
                                List<DoctorsOffice>  allOfficesWithAspecialty, ReportChoose reportChoose) {
        Report report = null;
        ReportChoose type = reportChoose;
        if (type != null) {
            switch (type) {
                case Specialty:
                    report = new SpecialtyReports(theUsers, theAppointmentsForEachUser, allOfficesWithAspecialty, type);
                    break;
                case Appointments:
                    report = new AppointmentsReport(theUsers, theAppointmentsForEachUser, allOfficesWithAspecialty, type);
                    break;
                default:
                    System.err.println("Unknown/unsupported report-type.");
            }
        } else {
            System.err.println("Undefined report-type.");
        }
        return report;
    }
}
