package com.example.T1.controllers;


import com.example.T1.models.Appointments;
import com.example.T1.services.implementation.AppointmentsServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 *  In clasa AppointmentsController sunt implementate metodele de apel corespunzatoare unui Appointment,
 *  facandu-se modificari in baza de date.
 *  Sunt apelate metodele din Service.
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class AppointmentsController {

    @Autowired
    AppointmentsServiceLogic appointmentsServiceLogic;

    /**
     * Metoda returnAppointments() va returna lista cu toate programarile.
     * @return lista de programari
     */
    @GetMapping("/getAllAppointments")
    public List<Appointments> returnAppointments ()
    {

        return appointmentsServiceLogic.returnAllAppointments();
    }

    /**
     * Metoda putAppointment() va insera o noua programare in tabelul "appointments".
     * @param firstAppointment - Entry-ul in tabelul "appointment"(o noua programare)
     */
    @PostMapping(path="/postAppointment", consumes="application/json", produces = "application/json")
    public void putAppointment (@RequestBody Appointments firstAppointment)
    {

        appointmentsServiceLogic.putAnAppointment(firstAppointment);
    }

    @DeleteMapping("/deleteAllApointments")
    public void deleteAllAppointments() {

        appointmentsServiceLogic.deleteAllAppointments();
    }



}
