package com.example.T1.facade;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
/** Clasa DoctorsOfficeFacade contine logica simpla pe repository-ul respectiv(operatii de get, post, update).
 * */
@Service
public class DoctorsOfficeFacade {

    @Autowired
    DoctorsOfficeRepository doctorsOfficeRepository;

    public DoctorsOfficeFacade() {

    }

    public DoctorsOfficeFacade(DoctorsOfficeRepository doctorsOfficeRepository) {
        this.doctorsOfficeRepository = doctorsOfficeRepository;
    }

    /**
     * Metoda findByName() va returna lista cu toate cabinetele medicale in functie de nume.
     * @return lista cabinetelor medicale
     */

    public DoctorsOffice findByName(String name){
        DoctorsOffice office = doctorsOfficeRepository.findByName(name);

        return office;
    }

    /**
     * Metoda returnAllOffices() va returna toate inregistrarile din tabel.
     */

    public List<DoctorsOffice> returnAllOffices ()
    {
        return doctorsOfficeRepository.findAll();
    }

    /**
     * Metoda returnOfficeById() va returna cabinetele in functie de id.
     */
    public Optional<DoctorsOffice> returnOfficeById(Long id) {

        return doctorsOfficeRepository.findById(id);

    }

    /***
     * Metoda returnOfficeByCAS() va returna cabinetele in functie de CAS.
     */

    public DoctorsOffice returnOfficeByCAS(String cas) {

        return doctorsOfficeRepository.findByCAS(cas);

    }

    /** Metoda putAnOffice() va adauga in tabel o noua inregistrare.
     * */
    public DoctorsOffice putAnOffice ( DoctorsOffice firstOffice)
    {

            return doctorsOfficeRepository.save(firstOffice);

    }


    /** Metoda replaceOffice() va face update cabinetelor in functie de numarul de programari disponibile.
     * */
    public DoctorsOffice replaceOffice(DoctorsOffice newOffice,Long id) {

        return doctorsOfficeRepository.findById(id)
                .map(office -> {
                    office.setAvailableAppointments(newOffice.getAvailableAppointments());
                    return doctorsOfficeRepository.save(office);
                })
                .orElseGet(() -> {
                    newOffice.setId(id);
                    return doctorsOfficeRepository.save(newOffice);
                });
    }

    /** Metoda deleteOffice() va sterge cabinete in functie de id.
     * */
    public void deleteOffice(Long id) {
        doctorsOfficeRepository.deleteById(id);
    }

    /** Metoda deleteAllOffices() va sterge toate inregistrarile.
     * */
    public void deleteAllOffices() {
        doctorsOfficeRepository.deleteAll();
    }


}
