package com.example.T1.facade;

import com.example.T1.models.User;
import com.example.T1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
/** Clasa UserFacade contine logica simpla pe repository-ul respectiv(operatii de get, post, update).
 * */
@Service
public class UserFacade {

     @Autowired
     UserRepository userRepository;

    public UserFacade() {

    }

    public UserFacade(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User findByUsernameUser(String name){
        User user = userRepository.findByUsername(name);


        return user;
    }

    /** Metoda returnUsers() va returna toate inregistrarile din tabel.
     * */
    public List<User> returnUsers(){
        return userRepository.findAll();
    }

    /** Metoda punAnUser() va adauga un nou user in tabel.
     * */
    public User putAnUser (User firstUser)
    {
            return userRepository.save(firstUser);

    }

    public void deleteAllTheUsers() {
        userRepository.deleteAll();
    }


}