package com.example.T1.services.implementation;

import com.example.T1.facade.AppointmentsFacade;
import com.example.T1.facade.DoctorsOfficeFacade;
import com.example.T1.facade.UserFacade;
import com.example.T1.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Clasa ReportServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 * Foloseste metodele din Facade, fara a utiliza direct Repository-ul.
 *
 */
@Service
public class ReportServiceLogic {

    @Autowired
    AppointmentsFacade appointmentsFacade;

    @Autowired
    DoctorsOfficeFacade doctorsOfficeFacade;

    @Autowired
    UserFacade userFacade;

    public ReportServiceLogic(){

    }

    public ReportServiceLogic(AppointmentsFacade appointmentsFacade, DoctorsOfficeFacade doctorsOfficeFacade, UserFacade userFacade) {
        this.appointmentsFacade = appointmentsFacade;
        this.doctorsOfficeFacade = doctorsOfficeFacade;
        this.userFacade = userFacade;
    }

    /**
     * Metoda getSpecialtyReport() are rolul de a cauta informatiile referitoare la cabinetele de o anumita specialitate.
     * @param specialty - tipul de specialitate cautata
     * @return String-ul cu informatiile necesare
     */
    public String getSpecialtyReport(String specialty)
    {
        List<DoctorsOffice> allOfficesWithAspecialty =new ArrayList<DoctorsOffice>();

        List<DoctorsOffice> allOffices = doctorsOfficeFacade.returnAllOffices();
        for(DoctorsOffice office : allOffices)
        {
            if(office.getSpecialty().equals(specialty))
                allOfficesWithAspecialty.add(office);
        }

        ReportFactory reportFactory = new ReportFactory();
        return reportFactory.createReport(null, null, allOfficesWithAspecialty, ReportChoose.Specialty).getChooseReport();

    }

    /**
     * Metoda getAppointmentsReport() are rolul de a cauta informatiile referitoare la cabinetele la care este programat un anumit user.
     * @param userId - id-ul user-ului cautat
     * @return  String-ul cu informatiile necesare
     */
    public String getAppointmentsReport(Long userId)
    {
        List<Appointments> allAppointments = new ArrayList<Appointments>();

        List<Appointments> appointments = appointmentsFacade.returnAppointments();

        for(Appointments appointment : appointments)
        {
            if(appointment.getId_user() == userId)
                allAppointments.add(appointment);
        }

        ReportFactory reportFactory = new ReportFactory();
        return reportFactory.createReport(null, allAppointments, null, ReportChoose.Appointments).getChooseReport();

    }
}
