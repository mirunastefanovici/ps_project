package com.example.T1.facade;

import com.example.T1.models.Appointments;
import com.example.T1.repositories.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
/** Clasa AppointmentsFacade contine logica simpla pe repository-ul respectiv(operatii de get, post, update).
 * */
@Service
public class AppointmentsFacade {

    @Autowired
    AppointmentRepository appointmentRepository;

    public AppointmentsFacade() {

    }

    public AppointmentsFacade(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }



    /**
     * Metoda findByUser() va returna lista cu toate programarile in functie de id_user.
     * @return programarea user-ului cu user_id
     */
    public Appointments findByUser(Long user_id){
        return appointmentRepository.findByUserId(user_id);
    }


    /** Metoda returnAppointments() va returna toate inregistrarile din tabel.
     * */
    public List<Appointments> returnAppointments(){
        return appointmentRepository.findAll();
    }

    /** Metoda putAnAppointment() va adauga o noua inregistrare tabel.
     * */
    public Appointments putAnAppointment (Appointments appointment)
    {
        return appointmentRepository.save(appointment);

    }

    /**
     * Metoda deleteAllAppointments() va sterge toate inregistrarile din tabel.
     */
    public void deleteAllAppointments() {
        appointmentRepository.deleteAll();
    }

}