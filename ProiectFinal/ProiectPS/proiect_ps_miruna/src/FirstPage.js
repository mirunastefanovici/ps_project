import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from "axios";
import {Button, Container, TextField} from  '@material-ui/core';

import { Grid } from "@material-ui/core";
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";

const columns = [
    {field: "name", headerName: "Nume cabinet", width: 190 },
    {field: "specialty", headerName: "Specialitate", width: 190 },
    {field: "doctorName", headerName: "Nume medic", width: 190 },
    {field: "controlPrice", headerName: "Pret consult", width: 190 },
    {field: "cas", headerName: "Contract cu CAS", width: 190 },
    {field: "availableAppointments", headerName: "Disponibilitate", width: 190 }
]
export default class DoctorsOffice extends React.Component{
    constructor (){
        super();
        this.state={
            offices : [],
            report : ""
            
        };
        
        axiosInstance
        .get("http://localhost:8080/getAllOffices")
        .then(     res => {
            const val = res.data;
            this.setState({offices: val})
            console.log("Afisarea cabinetelor")
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })
        
    }
    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    filtreOfficesSpecialty = (nume) => {
        var cabinet = null

        this.state.offices.forEach( e => {
            if( e.specialty.includes(nume))
                {
                    cabinet = e;
                    
                }
        })

        return cabinet;

    }
    filtreOffices = (nume) => {
        var cabinet = null

        this.state.offices.forEach( e => {
            if( e.name.includes(nume))
                {
                    cabinet = e;
                    
                }
        })

        return cabinet;

    }

    handleClick = () => {
        var elem = document.getElementById("numeCabinet")
        var numeCabinet = elem.value
        
        var cabinetId = this.filtreOffices(numeCabinet).id
        var idUser = localStorage.getItem("id_user")
        var cabinetDispo = this.filtreOffices(numeCabinet).availableAppointments - 1
        console.log(numeCabinet, idUser,cabinetId,cabinetDispo)
        
        axiosInstance.post("http://localhost:8080/postAppointment", {
            id_office : cabinetId,
            id_user : idUser
        })

        axiosInstance.put("http://localhost:8080/updateOffice/"+cabinetId, {
            name:numeCabinet ,
            specialty:this.filtreOffices(numeCabinet).specialty,
            doctorName:this.filtreOffices(numeCabinet).doctorName,
            controlPrice:this.filtreOffices(numeCabinet).controlPrice,
            cas:this.filtreOffices(numeCabinet).cas,
            availableAppointments: cabinetDispo
        } )

        var values = []
        axiosInstance
        .get("http://localhost:8080/getAllOffices")
        .then(     res => {
            values = res.data
            this.setState({offices: values})
        })
        .catch(error =>{
            console.log(error);
        })

    }

    handleClick2 = () => {
        var elem = document.getElementById("specialitate");
        var numeSpecialitate = elem.value;
        
       
        axiosInstance.get("http://localhost:8080/reportSpecialty/"+numeSpecialitate)
        .then(     res => {
            const val = res.data;
            this.setState({report: val})
            console.log(this.state.report)
            
        })
        .catch(error =>{
            console.log(error);
        })
        
    }

    render (){
        return (
            <div>

                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <div style={{ display: 'flex', height: 600,width:2000 }}>
                        <div style={{ flexGrow:6 }}>
                            <DataGrid rows={this.state.offices} columns={columns} pageSize={20}/>
                        </div>

                    </div>
               
                </div>
                <div style={{ display: 'flex-start', height: 400,width:500 }}>
                <Container maxWidth="xs">
                      <div>
                         <Grid>
                                <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="numeCabinet"
                                label="Introduceti numele cabinetului"
                                name="Introduceti numele cabinetului"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                                />

                                <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="numeSpecialitate"
                                label="Introduceti numele specialitatii"
                                name="Introduceti numele specialitatii"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                                />

                                <div style={{marginTop:30}}>
                                <Link to="/GoodbyePage">
                                <Button
                                onClick = { this.handleClick}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                >
                                Fa o programare!
            
                                 </Button>
                                 </Link>
                                 </div>

                                 <div style={{marginTop:30}}>
                                
                                <Button
                                onClick = { this.handleClick2}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                >
                                Vezi cabinetele medicale de o anumita specialitate!
                                 </Button>
                                
                                 </div>

                                 <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="specialitate"
                                label="Introduceti specialitatea"
                                name="Introduceti specialitatea"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                                />

<div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <div style={{ display: 'flex', height: 600,width:2000 }}>
                        <div style={{ flexGrow:6 }}>
                            <p>{this.state.report}</p>
                        </div>

                    </div>
               
                </div>

                            </Grid>
                        </div>
                </Container>



                </div>
            </div>
        );
    }

    
}

