import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import axiosInstance from "axios";
import { Grid } from "@material-ui/core";
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";


class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            id: 0,
            firstName : "",
            lastName : "",
            username: "",
            password: "",
        };
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
        console.log(value);
    };

    onSubmitFun = event => {
        event.preventDefault();
        let credentilas = {
            id : 7 ,
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            username: this.state.username,
            password: this.state.password,
            notified : 0
        }

        axiosInstance.post("http://localhost:8080/postUser", credentilas)
            .then(     res => {
                const val = res.data;
                console.log("Succes");
                this.props.history.push("/UserLogIn");
            })
            .catch(error => {
                console.log(error)
            })
    }


    render() {
        return (
            <Container maxWidth="sm">
                <div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                        <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="firstName"
                                label="Prenume"
                                name="firstName"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="lastName"
                                label="Nume"
                                name="lastName"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Nume de utilizator"
                                name="username"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Parola"
                                type="password"
                                id="password"
                                onChange={this.handleInput}
                                autoComplete="current-password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Creeaza un cont!
                </Button>
                <div style={{marginTop:30}}>
                <Link to="/UserLogIn">
                <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Intra in contul tau!
                </Button>
                </Link>
                </div>
                        </form>
                    </Grid>
                </div>
            </Container>
        );
    }

}

export default Login;