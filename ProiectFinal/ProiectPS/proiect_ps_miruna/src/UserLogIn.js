import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import axiosInstance from "axios";
import { Grid } from "@material-ui/core";

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
        };
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
        console.log(value);
    };

    onSubmitFun = event => {
        event.preventDefault();

        axiosInstance.get("http://localhost:8080/getVerifiedUser/"+this.state.username+"/"+this.state.password)
            .then(
                res => {
                    const val = res.data;
                    console.log(val);
                    if(val!=="")
                    {
                        console.log("Succes");
                        localStorage.setItem("id_user", val.id);
                        //localStorage.getItem("id_user");
                        this.props.history.push("/FirstPage");
                    }
                    
                }
            )
            .catch(error => {
                console.log(error)
            })
    }


    render() {
        return (
            <Container maxWidth="sm">
                <div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Nume de utilizator"
                                name="username"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Parola"
                                type="password"
                                id="password"
                                onChange={this.handleInput}
                                autoComplete="current-password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                               Intra in contul tau!
                </Button>
                        </form>
                    </Grid>
                </div>
            </Container>
        );
    }

}

export default Login;