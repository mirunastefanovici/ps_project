import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import User from './User'
import UserLogIn from './UserLogIn'
import FirstPage from './FirstPage'
import GoodbyePage from './GoodbyePage'

function App() {

  const defaultRoute = window.location.pathname === "/" ? <Redirect to="/User" /> : undefined;

  return (
   <Router>
     <Switch>
       <Route exact path="/User" component={User}/>
     </Switch>

     <Switch>
       <Route exact path="/UserLogIn" component={UserLogIn}/>
     </Switch>

     <Switch>
       <Route exact path="/FirstPage" component={FirstPage}/>
     </Switch>

     <Switch>
       <Route exact path="/GoodbyePage" component={GoodbyePage}/>
     </Switch>

     {defaultRoute}
   </Router>
  );
}

export default App;
