package com.example.T1.changesTest;

import com.example.T1.models.User;
import com.example.T1.repositories.UserRepository;
import com.example.T1.services.implementation.UserServiceLogic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class UserChangesTests {

    @Mock
    UserRepository userRepository;

    @Test
    public void testSameUsername()
    {

        UserServiceLogic userService = new UserServiceLogic(userRepository);
        User user = new User();

        user.setId((long) 2);
        user.setFirstName("Andreea");
        user.setLastName("Popescu");
        user.setUsername("andreea.popescu");
        user.setPassword("pass1");

        boolean check = userService.verifyUserName(user.getUsername());
        verify(userRepository, atLeastOnce()).findByUsername(user.getUsername());
    }

    @Test
    public void testSavingDoctorOffice(){
        userRepository = mock(UserRepository.class);

        User user = new User();

        user.setId((long) 2);
        user.setFirstName("Andreea");
        user.setLastName("Popescu");
        user.setUsername("andreea.popescu");
        user.setPassword("pass1");

        userRepository.save(user);
        verify(userRepository,atLeastOnce()).save(user);
    }

}
