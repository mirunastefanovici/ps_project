package com.example.T1.changesTest;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import com.example.T1.services.implementation.DoctorsOfficeServiceLogic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class DoctorsOfficeChangesTests {

    @Mock
    DoctorsOfficeRepository doctorsOfficeRepository;

    @Test
    public void testSameName()
    {

        DoctorsOfficeServiceLogic officeService = new DoctorsOfficeServiceLogic(doctorsOfficeRepository);
        DoctorsOffice office = new DoctorsOffice();

        office.setId((long) 2);
        office.setAvailableAppointments(5);
        office.setCas("yes");
        office.setControlPrice(100);
        office.setSpecialty("Cardiologie");
        office.setName("ORL Gorj");
        office.setDoctorName("Neamtu Tatiana");

        boolean check = officeService.verifyDoctorsOfficeName(office.getName());
        verify(doctorsOfficeRepository, atLeastOnce()).findByName(office.getName());
    }

    @Test
    public void testSavingDoctorOffice(){
        doctorsOfficeRepository = mock(DoctorsOfficeRepository.class);

        DoctorsOffice office = new DoctorsOffice();
        office.setId((long) 2);
        office.setAvailableAppointments(5);
        office.setCas("yes");
        office.setControlPrice(100);
        office.setSpecialty("Cardiologie");
        office.setName("ORL Gorj");
        office.setDoctorName("Neamtu Tatiana");

        doctorsOfficeRepository.save(office);
        verify(doctorsOfficeRepository,atLeastOnce()).save(office);
    }

}
