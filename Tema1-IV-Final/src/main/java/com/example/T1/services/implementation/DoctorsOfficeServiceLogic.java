package com.example.T1.services.implementation;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import com.example.T1.services.Interfaces.DoctorsOfficeService;

/**
 * Clasa DoctorsOfficeServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 */
public class DoctorsOfficeServiceLogic implements DoctorsOfficeService{

    private final DoctorsOfficeRepository doctorsOfficeRepository;

    /**
     * Constructorul clasei, ce primeste ca parametru un Repository
     * @param doctorsOfficeRepository
     */
    public DoctorsOfficeServiceLogic(DoctorsOfficeRepository doctorsOfficeRepository) {
        this.doctorsOfficeRepository = doctorsOfficeRepository;
    }

    /**
     * Metoda verifyDoctorsOfficeName() are rolul de a verifica unicitatea
     * unui cabinet medical in functie de nume.
     * @param name - numele cautat
     * @return true daca nu mai exista un cabinet cu acelasi nume, false in caz contrar
     */
    public boolean verifyDoctorsOfficeName(String name){
        DoctorsOffice office = doctorsOfficeRepository.findByName(name);

        if (office == null)
            return true;

        return false;
    }

}
