package com.example.T1.services.implementation;


import com.example.T1.models.User;
import com.example.T1.repositories.UserRepository;
import com.example.T1.services.Interfaces.UserService;

/**
 * Clasa UserServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 */
public class UserServiceLogic implements UserService {

    private final UserRepository userRepository;

    /**
     * Constructorul clasei, ce primeste ca parametru un Repository
     * @param userRepository
     */
    public UserServiceLogic(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Metoda verifyUserName() are rolul de a verifica unicitatea
     * unui User in functie de userame.
     * @param name - username-ul cautat
     * @return true daca nu mai exista un User cu acelasi username, false in caz contrar
     */
    public boolean verifyUserName(String name){
        User user = userRepository.findByUsername(name);

        if (user == null)
            return true;

        return false;
    }

}