package com.example.T1.controllers;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import com.example.T1.services.implementation.DoctorsOfficeServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 *  In clasa DoctorsOfficeController sunt implementate metodele de apel corespunzatoare unei entitati
 *  de tip DoctorsOffice(cabinet medical), facandu-se modificari in baza de date.
 *
 */
@RestController
public class DoctorsOfficeController {

    @Autowired
    DoctorsOfficeRepository anOffice;

    /**
     * Metoda returnOffice() va returna lista cu toate cabinetele medicale.
     * @return lista cabinetelor medicale
     */
    @GetMapping("/getAllOffices")
    public List<DoctorsOffice> returnOffice ()
    {
        return anOffice.findAll();
    }

    /**
     * Metoda returnById() va returna lista cu toate cabinetele medicale, in functie de ID.
     * @return lista cabinetelor medicale in functie de ID
     */
    @GetMapping("/getOfficeById/{id}")
    public Optional<DoctorsOffice> returnById(@PathVariable Long id) {

        return anOffice.findById(id);

    }

    /**
     * Metoda returnByCAS() va returna lista cu toate cabinetele medicale, in functie de contractul cu CAS.
     * @return lista cabinetelor medicale in functie ontractul cu CAS
     */
    @GetMapping("/getOfficeByCAS/{cas}")
    public DoctorsOffice returnByCAS(@PathVariable String cas) {

        return anOffice.findByCAS(cas);

    }

    /**
     * Metoda putOffice() va insera un nou cabinet medical in tabelul "doctors_office".
     * @param firstOffice - Entry-ul in tabelul "doctors_office"(noul cabinet medical)
     * @return Cabinetul medical adaugat, daca respecta conditiile, altfel NULL
     */
    @PostMapping(path="/postOffice", consumes="application/json", produces = "application/json")
    public DoctorsOffice putOffice (@RequestBody DoctorsOffice firstOffice)
    {
        DoctorsOfficeServiceLogic doctorsOfficeService = new DoctorsOfficeServiceLogic(anOffice);
        if(doctorsOfficeService.verifyDoctorsOfficeName(firstOffice.getName()))
        return anOffice.save(firstOffice);

        return null;
    }

    /**
     * Metoda replaceOffice() va face un update la un entry din tabelul doctors_office si
     * anume va modifica numarul de programari disponibile.
     * @param newOffice - Cabinetul cu noile modificari
     * @param id - id-ul cabinetului care trebuie modificat
     * @return cabinetul modificat
     */
    @PutMapping(path="/updateOffice/{id}", consumes="application/json", produces = "application/json")
    public DoctorsOffice replaceOffice(@RequestBody DoctorsOffice newOffice, @PathVariable Long id) {

        return anOffice.findById(id)
                .map(office -> {
                    office.setAvailableAppointments(newOffice.getAvailableAppointments());
                    return anOffice.save(office);
                })
                .orElseGet(() -> {
                    newOffice.setId(id);
                    return anOffice.save(newOffice);
                });
    }

    /**
     * Metoda deleteOffice() sterge un entry din tabelul doctors_office in functie de ID.
     * @param id - id-ul cabinetului care trebuie sters
     */
    @DeleteMapping("/deleteOffice/{id}")
    public void deleteOffice(@PathVariable Long id) {
        anOffice.deleteById(id);
    }
}
