package com.example.T1.controllers;

import com.example.T1.models.User;
import com.example.T1.repositories.UserRepository;
import com.example.T1.services.implementation.UserServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  In clasa UserController sunt implementate metodele de apel corespunzatoare unui User,
 *  facandu-se modificari in baza de date.
 *
 */
@RestController
public class UserController {

    @Autowired
    UserRepository anUser;


    /**
     * Metoda returnUser() va returna lista cu toti utilizatorii aplicatiei.
     * @return lista de utilizatori
     */
    @GetMapping("/getAllUsers")
    public List<User> returnUser ()
    {
        return anUser.findAll();
    }

    /**
     * Metoda putUser() va insera un nou utilizator in tabelul "user".
     * @param firstUser - Entry-ul in tabelul "user"(noul utilizator)
     * @return User-ul adaugat, daca respecta conditiile, altfel NULL
     */
    @PostMapping(path="/postUser", consumes="application/json", produces = "application/json")
    public User putUser (@RequestBody User firstUser)
    {
        UserServiceLogic userService = new UserServiceLogic(anUser);
        if(userService.verifyUserName(firstUser.getUsername()))
            return anUser.save(firstUser);

        return null;
    }
}
