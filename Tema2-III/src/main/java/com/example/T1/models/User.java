package com.example.T1.models;

import javax.persistence.*;

/**
 * User este clasa reprezentanta pentru informatiile legate de utilizatorii aplicatiei.
 * Fiecare utilizator va avea: un id, un  nume de familie, un prenume, un username si o parola.
 *
 */
@Entity
@Table
public class User implements NewsUserInterface{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 45)
    private String firstName;

    @Column(nullable = false, length = 45)
    private String lastName;

    @Column(nullable = false, length = 45)
    private String username;

    @Column(nullable = false, length = 45)
    private String password;

    private boolean notified = false;
    public boolean getVerifyObserver() {
        return notified;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /** Notificarea pe care o va primi fiecare user.
     * Variabila notified se va schimba atunci cand un user este notificat.
     * */
    @Override
    public void update() {
        notified = true;
        System.out.println(this.getLastName()+", un nou cabinet a fost adaugat.");
    }
}