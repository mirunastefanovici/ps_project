package com.example.T1.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    UserRepository anUser;

    @GetMapping("/helloUser")
    public User returnUser ()
    {
        return new User();
    }

    @PostMapping(path="/putUser", consumes="application/json", produces = "application/json")
    public String putUser (@RequestBody User firstUser)
    {

        anUser.save(firstUser);
        return "newPage";

    }
}
