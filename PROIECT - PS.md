# PROIECT - PS
## Aplicatie pentru programari medicale

     Aplicatia are rolul de a aduce un ajutor in plus in procesul
    de cautare al unui loc disponibil pentru un control la un cabinet in
    functie de problemele medicale.

## Functionalitati:

- creare cont si autentificare
- vizualizarea specializarilor medicale pentru care sunt valide programarile
- vizualizarea cabinetelor medicale si a informatiilor corespunzatoare acestora
- crearea unei programari pentru un loc disponibil
- vizualizarea unei pagini cu scop informativ despre anumite probleme medicale

## Implementare initiala 
     Aplicatia utilizeaza RESTful API, respectand arhitectura REST ce are la baza
    o serie de reguli din punct de vedere arhitectural.
    
**Implementarea logicii este structurata pe pachete**:
- pachetul "controllers" : clasele UserController, DoctorsOfficeControler
- pachetul "models" : clasele User, DoctorsOffice
- pachetul "repositories": clasele UserRepository, DoctorsOfficeRepository
- pachetul "services" ce contine subpachetele "implementation", "interfaces"
- pachetul "implementation": clasele UserServiceLogic, DoctorsOfficeServiceLogic
- pachetul "changesTest" : clasele DoctorsOfficeChangesTest, UserChangesTes
- pachetul "modelsTest" : clasele UserRepositoryTest, DoctorsOfficeRepositoryTest

## **Pachetul MODELS**
     Clasele User si DoctorOffice sunt clasele reprezentante pentru informatiile legate
    de utilizatori si cabinetele medicale disponibile.
## **Pachetul CONTROLLERS**
     Clasele UserController si DoctorsOfficeControler sunt clasele in care am implementat
    metodele de apel corespunzatoare acestor entitati : GET, POST, PUT, DELETE.
    
     In urma apelurilor, se vor observa modificarile aferente metodelor apelate.
## **Pachetul REPOSITORIES**
      Clasele UserRepository si DoctorsOfficeRepository fac conexiunea cu tabelele
     din baza de date. Pe langa metodele predefinite, am implementat si metode noi 
     in functie de necesitatile impuse de logica aplicatiei. De exemplu, una dintre aceste
     metode este findByCAS() ce selecteaza toate cabinetele inscrise in tabel ce au contract
     cu CAS.
## **Pachetul SERVICES**
     Clasele UserServiceLogic si DoctorsOfficeServiceLogic au rolul de a separa
    logica verificarilor de logica din Controller, ajutand la o mai buna organizare si
    intelegere a responsabilitatilor fiecarei entitati. 
## **Pachetul MODELSTEST**
     Acest pachet contine clasele in care sunt implementate teste ce verifica inserarea
    unui nou entry in tabel. Fiecare clasa, reprezentativa pentru fiecare tabel, contine     aceasta metoda de verificare.
## **Pachetul CHANGESTEST**
    Acest pachet contine clase de test in care sunt implementate teste ce verifica logica
    din clasele de service. Se verifica apelarea metodelor findByName(), findByUsername(), 
    save().

**Concluzii:**
- Am reusit conexiunea cu baza de date.
- Am reusit sa testez metodele de apel pe baza de date.
- Am reusit sa fac teste pentru apelarea unor metode si pentru inserarea in baza de date.
- Poate exista o evolutie ulterioara.
    

                                                    Stefanovici Miruna-Andreea
    
    
    





















