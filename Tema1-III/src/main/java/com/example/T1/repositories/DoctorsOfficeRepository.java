package com.example.T1.repositories;

import com.example.T1.models.DoctorsOffice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorsOfficeRepository extends JpaRepository<DoctorsOffice, Long> {

}