package com.example.T1.repositories;

import com.example.T1.models.DoctorsOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Clasa DoctorsOfficeRepository face conexiunea cu tabelul doctors_office din baza de date.
 */
@Repository
public interface DoctorsOfficeRepository extends JpaRepository<DoctorsOffice, Long> {

    /**
     * Functia findByCas() are ca implementare Query-ul ce selecteaza un entry
     * din tabelul doctors_office in functie de CAS.
     * @param cas - existenta unui contract cu casa de asigurari
     * @return entry-urile care respecta conditia
     */
    @Query("SELECT u FROM DoctorsOffice u WHERE u.cas = ?1")
    public DoctorsOffice findByCAS(String cas);

    /**
     * Functia findByName() are ca implementare Query-ul ce selecteaza un entry
     * din tabelul doctors_office in functie de nume.
     * @param name - numele cabinetului
     * @return entry-urile care respecta conditia
     */
    @Query("SELECT u FROM DoctorsOffice u WHERE u.name = ?1")
    public DoctorsOffice findByName(String name);
}