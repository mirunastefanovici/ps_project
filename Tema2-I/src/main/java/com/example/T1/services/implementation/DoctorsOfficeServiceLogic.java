package com.example.T1.services.implementation;


import com.example.T1.facade.DoctorsOfficeFacade;
import com.example.T1.facade.UserFacade;
import com.example.T1.models.DoctorsOffice;
import com.example.T1.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Clasa DoctorsOfficeServiceLogic are rolul de a separa logica de "business"(ce ulterior va deveni mai complexa)
 * de Controller printr-un nou layer.
 */
@Service
public class DoctorsOfficeServiceLogic {

    @Autowired
    DoctorsOfficeFacade doctorsOfficeFacade;

    @Autowired
    private UserFacade userFacade;

    private boolean verifyObserver = false;

    public boolean getVerifyObserver() {
        return verifyObserver;
    }

    public DoctorsOfficeServiceLogic() {

    }

    public DoctorsOfficeServiceLogic(DoctorsOfficeFacade doctorsOfficeFacade) {
        this.doctorsOfficeFacade = doctorsOfficeFacade;
    }

    public DoctorsOfficeServiceLogic(DoctorsOfficeFacade doctorsOfficeFacade,UserFacade userFacade ) {
        this.doctorsOfficeFacade = doctorsOfficeFacade;
        this.userFacade = userFacade;
    }


    private List<User> users = new ArrayList<>();
    /**
     * Metoda verifyDoctorsOfficeName() are rolul de a verifica unicitatea
     * unui cabinet medical in functie de nume.
     * @param name - numele cautat
     * @return true daca nu mai exista un cabinet cu acelasi nume, false in caz contrar
     */
    public boolean verifyDoctorsOfficeName(String name){

        DoctorsOffice theOffice = doctorsOfficeFacade.findByName(name);
        if( theOffice == null)
            return true;
        return false;
    }

    public void putUsersFromdatabase(){

        users = userFacade.returnUsers();
    }

    public List<DoctorsOffice> returnAllOffices(){
        return doctorsOfficeFacade.returnAllOffices();
    }

    public Optional<DoctorsOffice> returnOfficeById(Long id) {

        return doctorsOfficeFacade.returnOfficeById(id);

    }

    public DoctorsOffice returnOfficeByCAS(String cas) {

        return doctorsOfficeFacade.returnOfficeByCAS(cas);

    }

    public DoctorsOffice putOffice (DoctorsOffice firstOffice)
    {

        if(this.verifyDoctorsOfficeName(firstOffice.getName())) {
            this.putUsersFromdatabase();
           for(User user : users)
               user.update();
            verifyObserver = true;
           return doctorsOfficeFacade.putAnOffice(firstOffice);
        }

        return null;
    }

    public void replaceOffice(DoctorsOffice newOffice, Long id) {

        doctorsOfficeFacade.replaceOffice(newOffice,id);
    }

    public void deleteOffice(Long id) {
        doctorsOfficeFacade.deleteOffice(id);
    }

    public void deleteAllOffices() {
        doctorsOfficeFacade.deleteAllOffices();
    }
}
