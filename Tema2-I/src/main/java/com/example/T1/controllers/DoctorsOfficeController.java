package com.example.T1.controllers;


import com.example.T1.models.DoctorsOffice;
import com.example.T1.services.implementation.DoctorsOfficeServiceLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 *  In clasa DoctorsOfficeController sunt implementate metodele de apel corespunzatoare unei entitati
 *  de tip DoctorsOffice(cabinet medical), facandu-se modificari in baza de date.
 *
 */
@RestController
public class DoctorsOfficeController {


    @Autowired
    DoctorsOfficeServiceLogic officeService;
    /**
     * Metoda returnOffice() va returna lista cu toate cabinetele medicale.
     * @return lista cabinetelor medicale
     */

    @GetMapping("/getAllOffices")
    public List<DoctorsOffice> returnOffice ()
    {


        return officeService.returnAllOffices();
    }

    /**
     * Metoda returnById() va returna lista cu toate cabinetele medicale, in functie de ID.
     * @return lista cabinetelor medicale in functie de ID
     */

    @GetMapping("/getOfficeById/{id}")
    public Optional<DoctorsOffice> returnById(@PathVariable Long id) {

        return officeService.returnOfficeById(id);

    }

    /**
     * Metoda returnByCAS() va returna lista cu toate cabinetele medicale, in functie de contractul cu CAS.
     * @return lista cabinetelor medicale in functie ontractul cu CAS
    */
    @GetMapping("/getOfficeByCAS/{cas}")
    public DoctorsOffice returnByCAS(@PathVariable String cas) {

        return officeService.returnOfficeByCAS(cas);

    }

    /**
     * Metoda putOffice() va insera un nou cabinet medical in tabelul "doctors_office".
     * @param firstOffice - Entry-ul in tabelul "doctors_office"(noul cabinet medical)
     */
    @PostMapping(path="/postOffice", consumes="application/json", produces = "application/json")
    public void putOffice (@RequestBody DoctorsOffice firstOffice)
    {

        officeService.putOffice(firstOffice);
    }

    /**
     * Metoda replaceOffice() va face un update la un entry din tabelul doctors_office si
     * anume va modifica numarul de programari disponibile.
     * @param newOffice - Cabinetul cu noile modificari
     * @param id - id-ul cabinetului care trebuie modificat
    */
    @PutMapping(path="/updateOffice/{id}", consumes="application/json", produces = "application/json")
    public void replaceOffice(@RequestBody DoctorsOffice newOffice, @PathVariable Long id) {

        officeService.replaceOffice(newOffice, id);
    }

    /**
     * Metoda deleteOffice() sterge un entry din tabelul doctors_office in functie de ID.
     * @param id - id-ul cabinetului care trebuie sters
     */
    @DeleteMapping("/deleteOffice/{id}")
    public void deleteOffice(@PathVariable Long id) {

        officeService.deleteOffice(id);
    }

    @DeleteMapping("/deleteAllOffices")
    public void deleteAllOffices() {

        officeService.deleteAllOffices();
    }

}
