package com.example.T1.facade;

import com.example.T1.models.User;
import com.example.T1.repositories.DoctorsOfficeRepository;
import com.example.T1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class UserFacade {

     @Autowired
     UserRepository userRepository;

    public UserFacade() {

    }

    public UserFacade(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByUsernameUser(String name){
        User user = userRepository.findByUsername(name);


        return user;
    }

    public List<User> returnUsers(){
        return userRepository.findAll();
    }

    public User putAnUser (User firstUser)
    {
            return userRepository.save(firstUser);

    }

}