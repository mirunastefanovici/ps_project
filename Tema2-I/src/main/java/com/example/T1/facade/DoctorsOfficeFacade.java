package com.example.T1.facade;

import com.example.T1.models.DoctorsOffice;
import com.example.T1.repositories.DoctorsOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DoctorsOfficeFacade {

    @Autowired
    DoctorsOfficeRepository doctorsOfficeRepository;

    public DoctorsOfficeFacade() {

    }

    public DoctorsOfficeFacade(DoctorsOfficeRepository doctorsOfficeRepository) {
        this.doctorsOfficeRepository = doctorsOfficeRepository;
    }

    public DoctorsOffice findByName(String name){
        DoctorsOffice office = doctorsOfficeRepository.findByName(name);

        return office;
    }

    public List<DoctorsOffice> returnAllOffices ()
    {
        return doctorsOfficeRepository.findAll();
    }

    public Optional<DoctorsOffice> returnOfficeById(Long id) {

        return doctorsOfficeRepository.findById(id);

    }

    public DoctorsOffice returnOfficeByCAS(String cas) {

        return doctorsOfficeRepository.findByCAS(cas);

    }

    public DoctorsOffice putAnOffice ( DoctorsOffice firstOffice)
    {

            return doctorsOfficeRepository.save(firstOffice);

    }


    public DoctorsOffice replaceOffice(DoctorsOffice newOffice,Long id) {

        return doctorsOfficeRepository.findById(id)
                .map(office -> {
                    office.setAvailableAppointments(newOffice.getAvailableAppointments());
                    return doctorsOfficeRepository.save(office);
                })
                .orElseGet(() -> {
                    newOffice.setId(id);
                    return doctorsOfficeRepository.save(newOffice);
                });
    }

    public void deleteOffice(Long id) {
        doctorsOfficeRepository.deleteById(id);
    }

    public void deleteAllOffices() {
        doctorsOfficeRepository.deleteAll();
    }
}
